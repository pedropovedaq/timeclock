'use strict';

angular.module('timeclockApp.auth', [
  'timeclockApp.constants',
  'timeclockApp.util',
  'ngCookies',
  'ngRoute'
])
  .config(function($httpProvider) {
    $httpProvider.interceptors.push('authInterceptor');
  });
