'use strict';

angular.module('timeclockApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/dash', {
        template: '<dash></dash>'
      });
  });
