'use strict';
(function(){

class DashComponent {

  constructor(Auth) {
    this.buttonText = "Punch In";
    this.getCurrentUser = Auth.getCurrentUser;
    this.date = new Date();
    this.hasLogged= false;
  }

  toggle(){
    if(this.buttonText==="Punch In"){
      this.last_punch = new Date();
      this.buttonText="Punch Out"
    }
    else{
      this.buttonText="Punch In"
      this.time_logged = (new Date() - this.last_punch)/1000;
      this.hasLogged= true;
    }
  }
  savePunchIn(){
    Auth.updateLastPunch(new Date());
  }

  saveTimeLogged(){
    Auth.addToTimeLogged(new Date() - this.getCurrentUser.last_punch);
  }





}

angular.module('timeclockApp')
  .component('dash', {
    templateUrl: 'app/dash/dash.html',
    controller: DashComponent,
    controllerAs: 'dash'
  });

})();
