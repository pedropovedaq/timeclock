'use strict';

describe('Component: DashComponent', function () {

  // load the controller's module
  beforeEach(module('timeclockApp'));

  var DashComponent, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($componentController, $rootScope) {
    scope = $rootScope.$new();
    DashComponent = $componentController('DashComponent', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    1.should.equal(1);
  });
});
