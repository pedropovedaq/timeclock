'use strict';

angular.module('timeclockApp.admin', [
  'timeclockApp.auth',
  'ngRoute'
]);
